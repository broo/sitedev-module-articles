<?php

require_once '../../../../../www/cms/prepend.php';

use Ext\Xml;
use Sitedev\Articles\Model\SourceType;

$page = new App\Cms\Back\Page();

if ($page->isAllowed()) {

    // Инициализация объекта

    $obj = null;

    if (!empty($_GET['id'])) {
        $obj = SourceType::getById($_GET['id']);
        if (!$obj) reload();

    } else if (array_key_exists('add', $_GET)) {
        $obj = new SourceType();
    }


    // Форма редактирования или добавления объекта

    if ($obj) {
        $form = App\Cms\Ext\Form::load(dirname(__FILE__) . '/form.xml');
        $form->fillWithObject($obj);

        foreach (SourceType::getStatuses() as $status) {
            $form->statusId->addOption($status['id'], $status['title']);
        }

        $form->run();

        if ($form->isSubmited() && $form->isSuccess()) {
            if ($form->isSubmited('delete')) {
                $obj->delete();

                App\Cms\Back\Log::logModule(
                    App\Cms\Back\Log::ACT_DELETE,
                    $obj->id,
                    $obj->getTitle()
                );

                App\Cms\Ext\Form::saveCookieStatus();
                redirect($page->getUrl('path'));

            } else {
                $obj->fillWithData($form->toArray());
                $obj->save();

                App\Cms\Back\Log::logModule(
                    $form->isSubmited('insert') ? App\Cms\Back\Log::ACT_CREATE : App\Cms\Back\Log::ACT_MODIFY,
                    $obj->id,
                    $obj->getTitle()
                );

                App\Cms\Ext\Form::saveCookieStatus();
                reload('?id=' . $obj->id);
            }
        }
    }


    // Статус обработки формы

    $formStatusXml = '';

    if (!isset($form) || !$form->isSubmited()) {
        $formStatusXml = App\Cms\Ext\Form::getCookieStatusXml(
            empty($obj) ? 'Выполнено' : 'Данные сохранены'
        );

        App\Cms\Ext\Form::clearCookieStatus();
    }


    // Внутренняя навигация

    $filterXml = '';

    foreach (SourceType::getList() as $item) {
        $filterXml .= $item->getBackOfficeXml();
    }

    $filterXml = Xml::node(
        'local-navigation',
        $filterXml,
        array('is-sortable' => 1)
    );


    // XML модуля

    $xml = $filterXml . $formStatusXml;
    $attrs = array('type' => 'simple', 'is-able-to-add' => 'true');

    if (empty($obj)) {
        if (App\Cms\Back\Section::get()->description) {
            $xml .= Xml::notEmptyNode('content', Xml::cdata(
                'html',
                '<p class="first">' . App\Cms\Back\Section::get()->description . '</p>'
            ));
        }

    } else if ($obj->getId()) {
        $attrs['id'] = $obj->id;
        $xml .= Xml::cdata('title', $obj->getTitle());
        $xml .= $form->getXml();

    } else {
        $attrs['is-new'] = 1;
        $xml .= Xml::cdata('title', 'Добавление');
        $xml .= $form->getXml();
    }

    $page->addContent(Xml::node('module', $xml, $attrs));
}

$page->output();
