<?php

/**
 * @todo Is "reference" word correct to use?
 * @todo Table to DB scheme
 * @todo SQL to init files
 * @todo How should I refer to DB object
 * @todo New form element object
 * @todo New form element template
 * @todo Angular.js for search form element
 * @todo Is there method for getting where condition (@see self::getReferencePosts)
 */

namespace Core\Sitedev\Articles\Model;

use App\Cms\Back\Office\NavFilter;
use App\Cms\Back\Office\NavFilter\Element;
use Core\Sitedev\Posts\Model as Core;
use Ext\Date;
use Ext\Db;
use Ext\Xml;
use Sitedev\Articles\Model as Impl;

abstract class Article extends Core\Post
{
    /** @var Impl\Tag[] */
    protected $_tags;

    /** @var Impl\Source[] */
    protected $_sources;

    /** @var Impl\Category */
    protected $_category;

    /**
     * @param \Sitedev\Articles\Model\Category $_category
     */
    public function setCategory($_category)
    {
        $this->_category = $_category;
    }

    /**
     * @return \Sitedev\Articles\Model\Category
     */
    public function getCategory()
    {
        if (!isset($this->_category)) {
            if ($this->getCategoryId()) {
                $this->setCategory(
                    Impl\Category::getById($this->getCategoryId())
                );

            } else $this->setCategory(false);
        }

        return $this->_category;
    }

    public function getCategoryId()
    {
        return $this->postCategoryId;
    }

    /** @var self[] */
    protected $_referencePosts;

    public function __construct()
    {
        parent::__construct();

        $this->addForeign(Impl\Category::createInstance());
        $this->addForeign(Impl\Author::createInstance(), 'author_id');
        $this->addAttr('uri', 'string');
    }

    public function update()
    {
        $this->uri = $this->computeUri();
        return parent::update();
    }

    public function create()
    {
        $this->uri = $this->computeUri();
        return parent::create();
    }

    public function computeUri()
    {
        $this->checkAndFillName();

        if ($this->getCategory()) $uri = rtrim($this->getCategory()->uri, '/');
        else $uri = '';

        $uri .= '/' . $this->name;
        return $uri;
    }

    public function getUri()
    {
        return $this->uri;
    }

    public function delete()
    {
        Impl\Tag::deleteObjectTags($this->id);
        Impl\Source::deleteObjectSources($this->id);

        return parent::delete();
    }

    public function getTags()
    {
        if (!isset($this->_tags)) {
            $this->_tags = $this->id
                         ? Impl\Tag::getObjectTags($this->id)
                         : array();
        }

        return $this->_tags;
    }

    public function getTagIds()
    {
        return array_keys($this->getTags());
    }

    public function updateTags(array $_tags)
    {
        return $this->id
             ? Impl\Tag::updateObjectTags($this->id, $_tags)
             : false;
    }

    public function getSources()
    {
        if (!isset($this->_sources)) {
            $this->_sources = $this->id
                            ? Impl\Source::getObjectSources($this->id)
                            : array();
        }

        return $this->_sources;
    }

    public function updateSources(array $_sources)
    {
        if ($this->id) {
            return Impl\Source::updateObjectSources(
                $this->id,
                $_sources,
                $this->getSources()
            );

        } else return false;
    }

    /**
     * @return NavFilter
     */
    public static function getCmsNavFilter()
    {
        $filter = new NavFilter(get_called_class());


        // Дата

        $filter->addElement(new Element\Date(
            'publish_date',
            'integer',
            'Дата публикации'
        ));


        // Заголовок

        $filter->addElement(new Element('title', 'Название'));


        // Категория

        $categories = Impl\Category::getList();

        if (count($categories) > 0) {
            $el = new Element\Multiple(Impl\Category::getPri(), 'Категория');

            foreach ($categories as $item)
                $el->addOption($item->id, $item->getTitle());

            $filter->addElement($el);
        }

        // Автор

        $authors = Impl\Author::getList();

        if (count($authors) > 0) {
            $el = new Element\Multiple('author_id', 'Автор');

            foreach ($authors as $item)
                $el->addOption($item->id, $item->getTitle());

            $filter->addElement($el);
        }


        // Статус

        $statuses = static::getStatuses();

        if (count($statuses) > 0) {
            $el = new Element\Multiple('status_id', 'Статус');

            foreach ($statuses as $item)
                $el->addOption($item['id'], $item['title']);

            $filter->addElement($el);
        }


        $filter->run();
        return $filter;
    }

    /**
     * @param string $_search
     * @param int $_limit
     * @return self[]
     */
    public static function searchCms($_search, $_limit = 20)
    {
        return static::getList(array(
            'title LIKE "%' . $_search . '%" OR name LIKE "%' . $_search . '%"'
        ), array(
            'limit' => $_limit
        ));
    }

    /**
     * @return self[]
     */
    public function getReferencePosts()
    {
        if (is_null($this->_referencePosts)) {
            $this->_referencePosts = array();

            if ($this->id) {
                $ids = Db::get()->getList('
                    SELECT reference_post_id
                    FROM reference_post
                    WHERE ' . $this->getWhere()
                );

                if ($ids) {
                   $this->_referencePosts = static::getList(array(
                        static::getPri() => $ids 
                    ));
                }                
            }
        }

        return $this->_referencePosts;
    }

    /**
     * @return int[]
     */
    public function getReferencePostIds()
    {
        return array_keys($this->getReferencePosts());
    }
    
    /**
     * @param string $_node
     * @param string|array $_xml
     * @param array $_attrs
     * @return string
     */
    public function getXml($_node = null, $_xml = null, $_attrs = null)
    {
        $xml = $_xml ?: '';
        $attrs = is_array($_attrs) ? $_attrs : [];

        if (!array_key_exists('uri', $attrs))
            $attrs['uri'] = $this->getUri();

        if ($this->getPublishDate())
            Xml::append($xml, Date::getXml($this->getPublishDate()));

        Xml::append($xml, Xml::notEmptyCdata('intro', $this->getIntro()));
        Xml::append($xml, Xml::notEmptyCdata('content', $this->getContent()));

        if ($this->getCategory())
            Xml::append($xml, $this->getCategory()->getXml());

        return parent::getXml($_node, $xml, $attrs);
    }

    public function getContent()
    {
        $content = parent::getContent();

        if (strpos($content, '<p ') === false) {
            return nl2br($content);

        } else {
            return $content;
        }
    }
}
