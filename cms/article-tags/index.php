<?php

require_once '../../../../../www/cms/prepend.php';

use Ext\Xml;
use Sitedev\Articles\Model\Tag;

$page = new App\Cms\Back\Page();

if ($page->isAllowed()) {

    // Инициализация объекта

    $obj = null;

    if (!empty($_GET['id'])) {
        $obj = Tag::getById($_GET['id']);
        if (!$obj) reload();

    } else if (array_key_exists('add', $_GET)) {
        $obj = new Tag();
    }


    // Форма редактирования или добавления объекта

    if ($obj) {
        $form = App\Cms\Ext\Form::load(dirname(__FILE__) . '/form.xml');
        $form->fillWithObject($obj);

        if ($obj->id && $form->hasElement('files')) {
            foreach ($obj->getFiles() as $file) {
                $form->files->addAdditionalXml($file->getXml());
            }
        }

        $form->run();

        if ($form->isSubmited() && $form->isSuccess()) {
            if ($form->isSubmited('delete')) {
                $obj->delete();

                App\Cms\Back\Log::logModule(
                    App\Cms\Back\Log::ACT_DELETE,
                    $obj->id,
                    $obj->getTitle()
                );

                App\Cms\Ext\Form::saveCookieStatus();
                redirect($page->getUrl('path'));

            } else {
                $obj->fillWithData($form->toArray());
                $obj->save();

                if ($form->hasElement('files') && $form->files->getValue()) {
                    foreach ($form->files->getValue() as $file) {
                        $obj->uploadFile($file['name'], $file['tmp_name']);
                    }

                    $obj->cleanFileCache();
                }

                App\Cms\Back\Log::logModule(
                    $form->isSubmited('insert') ? App\Cms\Back\Log::ACT_CREATE : App\Cms\Back\Log::ACT_MODIFY,
                    $obj->id,
                    $obj->getTitle()
                );

                App\Cms\Ext\Form::saveCookieStatus();
                reload('?id=' . $obj->id);
            }
        }
    }


    // Статус обработки формы

    $formStatusXml = '';

    if (!isset($form) || !$form->isSubmited()) {
        $formStatusXml = App\Cms\Ext\Form::getCookieStatusXml(
            empty($obj) ? 'Выполнено' : 'Данные сохранены'
        );

        App\Cms\Ext\Form::clearCookieStatus();
    }


    // XML модуля

    $xml = Tag::getCmsNavFilter()->getXml() . $formStatusXml;
    $attrs = array('type' => 'simple', 'is-able-to-add' => 'true');

    if (empty($obj)) {
        if (App\Cms\Back\Section::get()->description) {
            $xml .= Xml::notEmptyNode('content', Xml::cdata(
                'html',
                '<p class="first">' . App\Cms\Back\Section::get()->description . '</p>'
            ));
        }

    } else if ($obj->getId()) {
        $attrs['id'] = $obj->id;
        $xml .= Xml::cdata('title', $obj->getTitle());
        $xml .= $form->getXml();

    } else {
        $attrs['is-new'] = 1;
        $xml .= Xml::cdata('title', 'Добавление');
        $xml .= $form->getXml();
    }

    $page->addContent(Xml::node('module', $xml, $attrs));
}

$page->output();
