<?php

require_once '../../../../../www/cms/prepend.php';

use \Ext\Xml;
use \Sitedev\Articles\Model;

$page = new App\Cms\Back\Page();

if ($page->isAllowed()) {

    // Инициализация объекта

    $obj = null;

    if (!empty($_GET['id'])) {
        $obj = Model\Article::getById($_GET['id']);
        if (!$obj) reload();

    } else if (array_key_exists('add', $_GET)) {
        $obj = new Model\Article();
    }


    // Форма редактирования или добавления объекта

    if ($obj) {
        /** @var App\Cms\Ext\Form $form */
        $form = App\Cms\Ext\Form::load(dirname(__FILE__) . '/form.xml');
        $form->fillWithObject($obj);

        foreach (Model\Article::getStatuses() as $status) {
            $form->statusId->addOption($status['id'], $status['title']);
        }


        $form->authorId->addOption(null, 'Без автора');

        foreach (Model\Author::getList() as $author) {
            $form->authorId->addOption($author->id, $author->getTitle());
        }


        $sourceTypes = Model\SourceType::getList();

        if ($sourceTypes) {
            foreach ($sourceTypes as $type) {
                $form->getGroup('sources')->addElement(
                    $form->createElement(
                        'source-type-' . $type->id,
                        'string',
                        $type->getTitle()
                    )
                );
            }

            foreach ($obj->getSources() as $source) {
                $el = $form->getElement('source-type-' . $source->sourceTypeId);

                if ($el) {
                    $el->setValue($source->value);
                }
            }

        } else {
            $form->deleteGroup('sources');
        }


        foreach (Model\Tag::getList() as $tag) {
            $form->tagIds->addOption($tag->id, $tag->getTitle());
        }

        if ($obj->id) {
            $form->tagIds->setValue($obj->getTagIds());

        } else {
            $form->publishDate->setValue(time());
        }

        $form->run();

        if ($form->isSubmited() && $form->isSuccess()) {
            if ($form->isSubmited('delete')) {
                $obj->delete();

                App\Cms\Back\Log::logModule(
                    App\Cms\Back\Log::ACT_DELETE,
                    $obj->id,
                    $obj->getTitle()
                );

                App\Cms\Ext\Form::saveCookieStatus();
                redirect($page->getUrl('path'));

            } else {
                $obj->fillWithData($form->toArray());


                // Проверка адреса на уникальность

                $uri = $obj->computeUri();
                if (!Model\Article::isUnique('uri', $uri, $obj->id)) {
                    $form->setUpdateStatus(Ext\Form::ERROR);
                    $form->name->setUpdateStatus(Ext\Form\Element::ERROR_EXIST);
                }


                if ($form->isSuccess()) {
                    $obj->save();

                    // Обновление тэгов

                    $tagIds = $form->tagIds->getValue();
                    if (!$tagIds) $tagIds = array();

                    foreach (
                        Ext\String::split($form->additionalTags->getValue()) as
                        $item
                    ) {
                        $tagIds[] = Model\Tag::getOrCreateIfUnique($item)->id;
                    }

                    $obj->updateTags(array_values(array_unique($tagIds)));


                    // Обновление ресурсов

                    if ($sourceTypes) {
                        $sources = array();

                        foreach ($sourceTypes as $type) {
                            $el = $form->getElement('source-type-' . $type->id);

                            if ($el && $el->getValue()) {
                                $sources[] = array(
                                    'type_id' => $type->id,
                                    'value' => $el->getValue()
                                );
                            }
                        }

                        $obj->updateSources($sources);
                    }

                    $actionId = $form->isSubmited('insert')
                              ? App\Cms\Back\Log::ACT_CREATE
                              : App\Cms\Back\Log::ACT_MODIFY;

                    App\Cms\Back\Log::logModule(
                        $actionId,
                        $obj->id,
                        $obj->getTitle()
                    );

                    App\Cms\Ext\Form::saveCookieStatus();
                    reload('?id=' . $obj->id);
                }
            }
        }
    }


    // Статус обработки формы

    $formStatusXml = '';

    if (!isset($form) || !$form->isSubmited()) {
        $formStatusXml = App\Cms\Ext\Form::getCookieStatusXml(
            empty($obj) ? 'Выполнено' : 'Данные сохранены'
        );

        App\Cms\Ext\Form::clearCookieStatus();
    }


    // XML модуля

    $xml = Model\Article::getCmsNavFilter()->getXml() . $formStatusXml;
    $attrs = array('type' => 'simple', 'is-able-to-add' => 'true');

    if (empty($obj)) {
        if (App\Cms\Back\Section::get()->description) {
            $xml .= Xml::notEmptyNode('content', Xml::cdata(
                'html',
                '<p class="first">' .
                App\Cms\Back\Section::get()->description .
                '</p>'
            ));
        }

    } else if ($obj->getId()) {
        $attrs['id'] = $obj->id;
        $xml .= Xml::cdata('title', $obj->getTitle());
        $xml .= $form->getXml();

    } else {
        $attrs['is-new'] = 1;
        $xml .= Xml::cdata('title', 'Добавление');
        $xml .= $form->getXml();
    }

    $page->addContent(Xml::node('module', $xml, $attrs));
}

$page->output();
