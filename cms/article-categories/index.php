<?php

require_once '../../../../../www/cms/prepend.php';

use Ext\Xml;
use Sitedev\Articles\Model\Category;

$page = new App\Cms\Back\Page();

if ($page->isAllowed()) {

    // Инициализация объекта

    $obj = null;

    if (!empty($_GET['id'])) {
        $obj = Category::getById($_GET['id']);
        if (!$obj) reload();

    } else if (array_key_exists('add', $_GET)) {
        $obj = new Category();
    }


    // Форма редактирования или добавления объекта

    if ($obj) {
        /** @var App\Cms\Ext\Form $form */
        $form = App\Cms\Ext\Form::load(dirname(__FILE__) . '/form.xml');
        $form->fillWithObject($obj);
        $form->run();

        if ($form->isSubmited() && $form->isSuccess()) {
            if ($form->isSubmited('delete')) {
                $obj->delete();

                App\Cms\Back\Log::logModule(
                    App\Cms\Back\Log::ACT_DELETE,
                    $obj->id,
                    $obj->getTitle()
                );

                App\Cms\Ext\Form::saveCookieStatus();
                redirect($page->getUrl('path'));

            } else {
                $obj->fillWithData($form->toArray());


                // Проверка адреса на уникальность

                $uri = $obj->computeUri();
                if (!Category::isUnique('uri', $uri, $obj->id)) {
                    $form->setUpdateStatus(Ext\Form::ERROR);
                    $form->name->setUpdateStatus(Ext\Form\Element::ERROR_EXIST);
                }


                if ($form->isSuccess()) {
                    $obj->save();
                    $actionId = $form->isSubmited('insert')
                              ? App\Cms\Back\Log::ACT_CREATE
                              : App\Cms\Back\Log::ACT_MODIFY;

                    App\Cms\Back\Log::logModule(
                        $actionId,
                        $obj->id,
                        $obj->getTitle()
                    );

                    App\Cms\Ext\Form::saveCookieStatus();
                    reload('?id=' . $obj->id);
                }
            }
        }
    }


    // Статус обработки формы

    $formStatusXml = '';

    if (!isset($form) || !$form->isSubmited()) {
        $formStatusXml = App\Cms\Ext\Form::getCookieStatusXml(
            empty($obj) ? 'Выполнено' : 'Данные сохранены'
        );

        App\Cms\Ext\Form::clearCookieStatus();
    }


    // XML модуля

    $xml = $formStatusXml;
    $attrs = array('type' => 'tree', 'is-able-to-add' => 'true');

    if (empty($obj)) {
        if (App\Cms\Back\Section::get()->description) {
            $xml .= Xml::notEmptyNode('content', Xml::cdata(
                'html',
                '<p class="first">' .
                App\Cms\Back\Section::get()->description .
                '</p>'
            ));
        }

    } else if ($obj->getId()) {
        $attrs['id'] = $obj->id;
        $xml .= Xml::cdata('title', $obj->getTitle());
        $xml .= $form->getXml();

    } else {
        $attrs['is-new'] = 1;
        $xml .= Xml::cdata('title', 'Добавление');
        $xml .= $form->getXml();
    }

    $page->addContent(Xml::node('module', $xml, $attrs));
}

$page->output();
